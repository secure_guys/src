#include "simpletools.h" // Libraries simpletools & keypad
#include "keypad.h"

int rows[4] = {7, 6, 5, 4}; // Row I/O pins (top to bottom)
int cols[4] = {3, 2, 1, 0}; // Column I/O pins (left to right)

int values[16] = { 1, 2, 3, 'A', // Values for each key in the 4x4
4, 5, 6, 'B',
7, 8, 9, 'C',
'*', 0, '#', 'D' };
int number1 = 0;
// Stores number result

int main() // Main function
{
keypad_setup(4, 4, rows, cols, values); // Setup dimensions, keypad arrays
print("Type the passcode, then press # \r"); // User prompt
while(1) // Main loop
{
number1 = keypad_getNumber(); // Get number entered on keypad
if(number1 == 1234)
{
print("\r"); 
print("Access Given\r"); 
// Next line
//print("The passcode is %d\r", number1);
} 
else
{
print("\r");
print("Access Denied");
} 
// Display result
} 
}