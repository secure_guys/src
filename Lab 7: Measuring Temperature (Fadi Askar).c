#include "simpletools.h" // Include simpletools
#include "adcDCpropab.h" // Include adcDCpropab

int main() // main function
{
adc_init(21, 20, 19, 18); // CS=21, SCL=20, DO=19, DI=18

float temp; // Voltage variables

while(1) // Loop repeats indefinitely
{
temp = adc_volts(3); // Check A/D 3
temp= temp*324;
putChar(HOME); // Cursor -> top-left "home"
print("temp = %f F%c\n", temp, CLREOL); // Display volts

pause(100); 
if(temp == 100)
{
print("\r"); 
print("Warning, High temperature\r"); 
// Next line
//print("The passcode is %d\r", number1);

} 
if(temp==50)
{
print("\r"); 
print("Warning, LOW temperature\r");
} 
else
{
print("\r");
print("Normal temperature");
} 

} 
}