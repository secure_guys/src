/*
Test WAV Volume.c

Play back a .wav file and try a few different volume settings. 
*/

#include "simpletools.h"
#include "wavplayer.h"
int main() // main function
{
while(1)
{
if( input(8)==1) // Endless loop
{

int DO = 22, CLK = 23, DI = 24, CS = 25; // SD I/O pins
sd_mount(DO, CLK, DI, CS); // Mount SD card
const char aclock[] = {"aclock.wav"}; // Set up techloop string
wav_play(aclock); // Pass to wav player
wav_volume(10); // Adjust volume
pause(3500); // Play for 3.5 s
wav_volume(10); // Repeat twice more
pause(2000);
wav_volume(10);
pause(3500);

wav_stop(); // Stop playing
} 
} 
}