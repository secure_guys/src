#include "simpletools.h" // Libraries included
#include "sirc.h"

int main() // main function
{
sirc_setTimeout(1000); // -1 if no remote code in 1 s

while(1) // Repeat indefinitely
{ 
// decoding signal from P10
print("%c remote button = %d%c", // Display button # decoded
HOME, sirc_button(10), CLREOL); // from signal on P10
int button = sirc_button(10);
if(button==1) high(2);
pause(100);
if(button==2) low(2);
pause(100); // 1/10 s before loop repeat
} 
}