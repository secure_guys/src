/*
Test SIRC Remote.c

Connect 38 kHz IR receiver to Propeller P10
Decodes button presses on Sony-compatible IR remote
Run with Terminal

*/

#include "simpletools.h" // Libraries included
#include "sirc.h"
#include "keypad.h"
#include "wavplayer.h"

//Fadi Askar 

void keypad();
void LightSensor();
void remote();
void ultra();

//int *keypadCog;

int rows[4] = {7, 6, 5, 4}; // Row I/O pins (top to bottom)
int cols[4] = {3, 2, 1, 0}; // Column I/O pins (left to right)

int values[16] = { 1, 2, 3, 'A', // Values for each key in the 4x4
4, 5, 6, 'B',
7, 8, 9, 'C',
'*', 0, '#', 'D' };
int number1 = 0;



int main () // main function
{

keypad_setup(4, 4, rows, cols, values); // Setup dimensions, keypad arrays
//simpleterm_open();
print("Type the passcode, then press # \r"); // User prompt

//simpleterm_close();
//cog_end(keypadCog);
//simpleterm_close();
//number1 = keypad_getNumber(); // Get number entered on keypad
cog_run(keypad,128);
  //simpleterm_close();                       // Close SimpleIDE Terminal for this core
  //keypadCog = cog_run(keypad, 128);
cog_run(LightSensor,128);
cog_run(remote,128);
cog_run(ultra,128);
}

void keypad()
//Fadi Askar
{
  /*
Keypad 4x4 Digits to Numbers.c
Demonstrates how to build numbers with multiple key presses. 
*/ 



// Stores number result

while(1) // Main loop
{

                       // Close SimpleIDE Terminal for this core
number1 = keypad_getNumber(); // Get number entered on keypad
if(number1 == 1234)
{
//print("\r"); 
//print("Access Given\r"); 
//{
freqout(12, 1000, 5000);

}
// Next line
//print("The passcode is %d\r", number1);
//} 
else
//Mohammed
{
//print("\r");
//print("Access Denied");
//{
freqout(12, 500, 3000);
freqout(12, 500, 3000);
freqout(12, 500, 3000);
freqout(12, 500, 3000);
}

//} 
// Display result
} 
}




//----------------------------- remote
void remote()
//Mohammed
{
sirc_setTimeout(1000); // -1 if no remote code in 1 s

while(1) // Repeat indefinitely
{ 
// decoding signal from P10
print("%c remote button = %d%c", // Display button # decoded
HOME, sirc_button(10), CLREOL); // from signal on P10
int button = sirc_button(10);
if(button==1) high(27);
pause(100);
if(button==2) low(27);
pause(100); // 1/10 s before loop repeat

}
}

// ---------------------- Light Sensor

void LightSensor()
//Hussam Khazali
{
  while(1) // Endless loop
{
high(11); // Set P11 high
pause(1); // Wait for circuit to charge
int t = rc_time(11, 1); // Measure decay time on P5
print("t = %d\n", t); // Display decay time
pause(100); 
if (t==0)
{
high(26); // Set P26 I/O pin high
pause(100); 
} 
else
low(26); // Set P26 I/O pin high
pause(100); // Wait 1/10th of a second
}
}
//------------------------- Ultra


void ultra()
//Husam khazali
{
  while(1){
float i = -1;
i = ping(9,8);
//print("%f\n", i/148); //divide the ping by 148 to get inches, or by 54 to get centimeters
pause(300);
if (i/148<2.0)
{
//Fadi Askar
int DO = 22, CLK = 23, DI = 24, CS = 25; // SD I/O pins
sd_mount(DO, CLK, DI, CS); // Mount SD card
const char aclock[] = {"aclock.wav"}; // Set up techloop string
wav_play(aclock); // Pass to wav player
wav_volume(10); // Adjust volume
pause(3500); // Play for 3.5 s
wav_volume(10); // Repeat twice more
pause(2000);
wav_volume(10);
pause(3500);

wav_stop(); // Stop playing

}
}

}

int ping(int trig, int echo)//trig is trigger pin, echo is echo pin
{
low(trig);//set trig low for start pulse
low(echo);//set echo low to be safe
pulse_out(trig,10);//send the minimum 10 ms pulse on trig to start a ping
return pulse_in(echo, 1);//get the pulse duration back on echo pin
}
